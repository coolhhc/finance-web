module Handler.Feed.CommitFeed (
  getCommitFeed
  ) where

import Import hiding (parseTime, replace)
import Network.Curl.Download 
import Data.Maybe
import Data.Time.Clock (UTCTime)
import Data.Time.Format (parseTime)
import Text.Feed.Types (Feed (..))
import Text.XML.Light.Types as X
import System.Locale


getCommitFeed :: String -> IO (Maybe [(String, Maybe UTCTime)])
getCommitFeed url = do
  feed <- openAsFeed url
  case feed of
    (Right r) -> return $ Just (extractCommitAndTime r)
    (Left _)  -> return Nothing

extractCommitAndTime :: Feed -> [(String, Maybe UTCTime)]
extractCommitAndTime (XMLFeed el) =
  let els  = el:[]
      els1 = el2cont2el els
      els2 = el2cont2el els1
      els3 = el2cont2el els2
      elsTitle    = filter (\elm -> qName (elName elm) == "title") els3
      elsTime     = filter (\elm -> qName (elName elm) == "pubDate") els3
      commits     = extractText elsTitle
      times       = extractText elsTime
      displayTime = parse2Time times
  in zip commits displayTime
extractCommitAndTime _ = error "error extractCommitTime: not XMLFeed"

el2cont2el :: [Element] -> [Element]
el2cont2el el = let 
  content  = concat (map elContent el)
  maybeEls = map extractEl content
  in liftMaybe (filter isJust maybeEls)

extractText :: [Element] -> [String]
extractText elWithText = let
  texts  = concat (map elContent elWithText)
  cdatas = liftMaybe (map extractD texts)
  in map cdData cdatas

parse2Time :: [String] -> [Maybe UTCTime]
parse2Time [] = []
parse2Time (time:times) = parseString time : parse2Time times

parseString :: String -> Maybe UTCTime
parseString time = case parseTime defaultTimeLocale "%a, %e %b %Y %T %z" time of
  Just t  -> Just (t :: UTCTime)
  Nothing -> Nothing
 
extractD :: X.Content -> Maybe CData
extractD (Text t) = Just t
extractD _        = Nothing

extractEl :: X.Content -> Maybe Element
extractEl (Elem e) = Just e
extractEl _        = Nothing

liftMaybe :: [Maybe a] -> [a]
liftMaybe [] = []
liftMaybe (x:xs) = (\(Just j) -> j) x : liftMaybe xs
