{-# LANGUAGE TupleSections, OverloadedStrings, QuasiQuotes #-}
module Handler.Home where

import Import
import Handler.Feed.CommitFeed (getCommitFeed)
import Data.Time.Clock (UTCTime)
import Data.Time.Format (formatTime)
import System.Locale

getHomeR :: Handler RepHtml
getHomeR = do
  feeds <- liftIO $ getCommitFeed "https://bitbucket.org/coolhhc/finance-web/rss?token=d63c3aa730e77632193bc544dcb14f17"
  defaultLayout $ do
    setTitle "Finance web page"
    $(widgetFile "header")
    $(widgetFile "welcome")
    $(widgetFile "footer")
    addScript $ StaticR js_jquery_js
    addScript $ StaticR js_bootstrap_js
    addStylesheet $ StaticR css_bootstrap_css
    addStylesheet $ StaticR css_header_css
    addStylesheet $ StaticR css_general_css
    addStylesheet $ StaticR css_footer_css

prettyTime :: UTCTime -> String
prettyTime = formatTime defaultTimeLocale "%B %e, %Y %r"
