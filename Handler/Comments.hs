{-# LANGUAGE TupleSections, OverloadedStrings, QuasiQuotes #-}
module Handler.Comments where

import Import
import Handler.Form.CommentForm (commentForm, CommentForm(..))
import System.Time
import Data.Time (getCurrentTime, diffUTCTime, NominalDiffTime)
import Data.Text as T (unpack)
import Database.Persist.Store

getCommentsR :: Handler RepHtml
getCommentsR = do
  (formWidget, formEnctype) <- generateFormPost commentForm
  let renderS = renderSecs . round :: NominalDiffTime -> String
  comments <- runDB $ selectList [CommentName !=. ""] []
  timeNow <- liftIO getCurrentTime
  defaultLayout $ do
    setTitle "Finance web page - Comments"
    addStylesheet $ StaticR css_bootstrap_css
    addStylesheet $ StaticR css_header_css
    addStylesheet $ StaticR css_general_css
    addStylesheet $ StaticR css_footer_css
    addScriptRemote "https://ajax.googleapis.com/ajax/libs/jquery/1.6.4/jquery.min.js"
    addScriptRemote "http://twitter.github.com/bootstrap/assets/js/google-code-prettify/prettify.js"
    addScriptRemote "http://twitter.github.com/bootstrap/1.4.0/bootstrap-modal.js"
    addScriptRemote "http://twitter.github.com/bootstrap/1.4.0/bootstrap-alerts.js"
    addScriptRemote "http://twitter.github.com/bootstrap/1.4.0/bootstrap-twipsy.js"
    addScriptRemote "http://twitter.github.com/bootstrap/1.4.0/bootstrap-popover.js"
    addScriptRemote "http://twitter.github.com/bootstrap/1.4.0/bootstrap-dropdown.js"
    addScriptRemote "http://twitter.github.com/bootstrap/1.4.0/bootstrap-scrollspy.js"
    addScriptRemote "http://twitter.github.com/bootstrap/1.4.0/bootstrap-tabs.js"
    addScriptRemote "http://twitter.github.com/bootstrap/1.4.0/bootstrap-buttons.js"
    toWidgetHead [julius| 
      $(document).ready(function() {
	    $('#commentDialog').bind('show', function () {
		});
	  });

	  function closeDialog () {
	    $('#commentDialog').modal('hide'); 
	  };
    |]
    $(widgetFile "header")
    $(widgetFile "comments")
    $(widgetFile "footer")

postCommentsR :: Handler RepHtml
postCommentsR = do
  ((result, _), _) <- runFormPost commentForm
  let formData = case result of
        FormSuccess res ->  Just res
        _               ->  Nothing
  let comment = case formData of 
        Just com -> com
        Nothing  -> (CommentForm "" (Textarea ""))
  now <- liftIO getCurrentTime
  _ <- runDB $ insert $ Comment (name comment) (post comment) now
  redirect $ CommentsR


-- Should get from library System.Time.Utils
renderSecs :: Integer -> String
renderSecs i = renderTD $ diffClockTimes (TOD i 0) (TOD 0 0)

renderTD :: TimeDiff -> String
renderTD itd =
  case workinglist of
    [] -> "0s"
    _  -> concat . map (\(q, s) -> show q ++ [s]) $ workinglist
  where td = normalizeTimeDiff itd
        suffixlist = "YMDHMS"
        quantlist = (\(TimeDiff y mo d h m s _) -> [y, mo, d, h, m, s]) td
        zippedlist = zip quantlist suffixlist
        workinglist = take 2 . dropWhile (\(q, _) -> q == 0) $ zippedlist
