{-# LANGUAGE TupleSections, OverloadedStrings #-}
module Handler.Form (
  getCompanyR,
  postCompanyR,
  getFormR,
  postFormR
  ) where

import Import
import Prelude as P
import Handler.Form.FinanceForm (financeForm)
import Handler.Form.CompanyForm (companyForm, name, stockId, author)
import Handler.CompanyComputations.FinanceFormProcessor (process, getInputsAndStore)
import Handler.CompanyComputations.StatusDisplay (getAllStatistic)
import Data.Time (getCurrentTime)
import Data.Text (pack, unpack)

getCompanyR :: Handler RepHtml
getCompanyR = do
  (formWidget, formEnctype) <- generateFormPost companyForm
  defaultLayout $ do
    setTitle "Finance web page"
    addStylesheet $ StaticR css_bootstrap_css
    addStylesheet $ StaticR css_header_css
    addStylesheet $ StaticR css_general_css
    addStylesheet $ StaticR css_footer_css
    $(widgetFile "header")
    $(widgetFile "companyForm")
    $(widgetFile "footer")

postCompanyR :: Handler RepHtml
postCompanyR = do
  ((companyResult, _), _) <- runFormPost companyForm
  let res = case companyResult of
        FormSuccess re ->  re
        _               ->  error "error companyResult not successful"
  companyId <- runDB $ insert $ Company (name res) (stockId res) (author res)
  setSession "companyId" (pack (show companyId))
  redirect $ FormR

getFormR :: Handler RepHtml
getFormR = do
  (formWidget, formEnctype) <- generateFormPost financeForm
  defaultLayout $ do
    setTitle "Finance web page"
    addStylesheet $ StaticR css_bootstrap_css
    addStylesheet $ StaticR css_header_css
    addStylesheet $ StaticR css_general_css
    addStylesheet $ StaticR css_footer_css
    $(widgetFile "header")
    $(widgetFile "financeForm")
    $(widgetFile "footer")

postFormR :: Handler RepHtml
postFormR = do
  ((result, formWidget), formEnctype) <- runFormPost financeForm
  now <- liftIO getCurrentTime
  (Just cId) <- lookupSession "companyId"
  let formData = case result of
                   FormSuccess re2 ->  Just re2
                   _               ->  Nothing 

      companyId               = read (unpack cId) :: (Key Company)
      (inputs, databaseStore) = getInputsAndStore formData companyId now  
      computedFinanceData     = process formData
      (expectedReturnAns, predictionTable, otherCalculations) = getAllStatistic computedFinanceData

  _ <- runDB $ insert $ databaseStore

  defaultLayout $ do
    setTitle "Finance web page"
    addStylesheet $ StaticR css_bootstrap_css
    addStylesheet $ StaticR css_header_css
    addStylesheet $ StaticR css_general_css
    addStylesheet $ StaticR css_footer_css
    $(widgetFile "header")
    $(widgetFile "displayCalculation")
    $(widgetFile "footer")
