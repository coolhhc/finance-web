module Handler.Form.FinanceForm (
  financeForm,
  FinanceForm(..) 
  ) where

import Import

data FinanceForm = FinanceForm {
  price_Per_Share :: Text,
  assets :: Text,
  shares_Outstanding :: Text,
  price_Earnings_Ratio_Estimate :: Text,
  non_Current_Assets_Now :: Text,
  non_Current_Assets_4YearAgo :: Text,
  profit_Now :: Text,
  profit_1YearAgo :: Text,
  profit_2YearAgo :: Text,
  profit_3YearAgo :: Text,
  dividend_Now :: Text,
  dividend_1YearAgo :: Text,
  dividend_2YearAgo :: Text,
  dividend_3YearAgo :: Text,
  equity_Now :: Text,
  equity_1YearAgo :: Text,
  equity_2YearAgo :: Text,
  equity_3YearAgo :: Text
  }
  deriving (Show, Eq) 

financeForm :: Html -> MForm App App (FormResult FinanceForm, Widget) 
financeForm = renderDivs $ FinanceForm
  <$> areq textField "Price_Per_Share" Nothing
  <*> areq textField "Assets" Nothing
  <*> areq textField "Shares_Outstanding" Nothing
  <*> areq textField "Price_Earnings_Ratio_Estimate" Nothing
  <*> areq textField "Non_Current_Assets_Now" Nothing
  <*> areq textField "Non_Current_Assets_4YearAgo" Nothing
  <*> areq textField "Profit_Now" Nothing
  <*> areq textField "Profit_1YearAgo" Nothing
  <*> areq textField "Profit_2YearAgo" Nothing
  <*> areq textField "Profit_3YearAgo" Nothing
  <*> areq textField "Dividend_Now" Nothing
  <*> areq textField "Dividend_1YearAgo" Nothing
  <*> areq textField "Dividend_2YearAgo" Nothing
  <*> areq textField "Dividend_3YearAgo" Nothing
  <*> areq textField "Equity_Now" Nothing
  <*> areq textField "Equity_1YearAgo" Nothing
  <*> areq textField "Equity_2YearAgo" Nothing
  <*> areq textField "Equity_3YearAgo" Nothing
