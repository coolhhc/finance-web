module Handler.Form.CommentForm (
  commentForm,
  CommentForm(..) 
  ) where

import Import

data CommentForm = CommentForm {
  name :: Text,
  post :: Textarea
  }
  deriving (Show, Eq) 

commentForm :: Html -> MForm App App (FormResult CommentForm, Widget) 
commentForm = renderDivs $ CommentForm
  <$> areq textField "Name" Nothing
  <*> areq textareaField "Post" Nothing
