module Handler.Form.CompanyForm (
  companyForm,
  CompanyForm(..) 
  ) where

import Import

data CompanyForm = CompanyForm {
  name :: Text,
  stockId :: Text,
  author :: Text
  }
  deriving (Show, Eq) 

companyForm :: Html -> MForm App App (FormResult CompanyForm, Widget) 
companyForm = renderDivs $ CompanyForm
  <$> areq textField "Company Name" Nothing
  <*> areq textField "Stock ID Symbol" Nothing
  <*> areq textField "Submit Author" Nothing
