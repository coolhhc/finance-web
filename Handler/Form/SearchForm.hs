
{-
 - NOT USED
 -}

module Handler.Form.SearchForm (
  searchForm,
  SearchForm(..) 
  ) where

import Import

data SearchForm = SearchForm {
  searchString :: Text
  }
  deriving (Show, Eq) 

searchForm :: Html -> MForm App App (FormResult SearchForm, Widget) 
searchForm = renderDivs $ SearchForm
  <$> areq textField "Search" Nothing
