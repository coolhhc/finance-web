{-# LANGUAGE TupleSections, OverloadedStrings, QuasiQuotes #-}
module Handler.Rankings where

import Import

getRankingsR :: Handler RepHtml
getRankingsR = do
  defaultLayout $ do
    setTitle "Finance web page - Rankings"
    addStylesheet $ StaticR css_bootstrap_css
    addStylesheet $ StaticR css_header_css
    addStylesheet $ StaticR css_general_css
    addStylesheet $ StaticR css_footer_css
    $(widgetFile "header")
    $(widgetFile "rankings")
    $(widgetFile "footer")

