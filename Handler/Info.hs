{-# LANGUAGE QuasiQuotes #-}
module Handler.Info where

import Import

getContactR :: Handler RepHtml
getContactR = do
  defaultLayout $ do
    setTitle "Finance web - contact us"
    addStylesheet $ StaticR css_bootstrap_css
    addStylesheet $ StaticR css_header_css
    addStylesheet $ StaticR css_general_css
    addStylesheet $ StaticR css_footer_css
    $(widgetFile "header")
    $(widgetFile "contact")
    $(widgetFile "footer")

getAboutR :: Handler RepHtml
getAboutR = do
  defaultLayout $ do
    setTitle "Finance web - contact us"
    addStylesheet $ StaticR css_bootstrap_css
    addStylesheet $ StaticR css_header_css
    addStylesheet $ StaticR css_general_css
    addStylesheet $ StaticR css_footer_css
    $(widgetFile "header")
    $(widgetFile "about")
    $(widgetFile "footer")

getAuthorR :: Handler RepHtml
getAuthorR = do
  defaultLayout $ do
    setTitle "Finance web - Author information"
    addStylesheet $ StaticR css_bootstrap_css
    addStylesheet $ StaticR css_header_css
    addStylesheet $ StaticR css_general_css
    addStylesheet $ StaticR css_footer_css
    $(widgetFile "header")
    $(widgetFile "author")
    $(widgetFile "footer")

getTermsR :: Handler RepHtml
getTermsR = do
  defaultLayout $ do
    setTitle "Finance web - Author information"
    addStylesheet $ StaticR css_bootstrap_css
    addStylesheet $ StaticR css_header_css
    addStylesheet $ StaticR css_general_css
    addStylesheet $ StaticR css_footer_css
    $(widgetFile "header")
    $(widgetFile "terms")
    $(widgetFile "footer")
