{-# LANGUAGE TupleSections, OverloadedStrings #-}
module Handler.Search (
  postSearchR
  ) where

import Import
import Handler.CompanyComputations.SearchTransform (searchTransform, searchInputs)
import Handler.CompanyComputations.StatusDisplay (getAllStatistic)
import MakeFData (makeFinanceData)
import Database.Persist.Store (PersistValue (PersistInt64))

data Search = Search {
  search :: Text
  }
  deriving Show

postSearchR :: Handler RepHtml
postSearchR = do

  searchWrapper <- runInputPost $ Search
                     <$> ireq textField "search"

  maybeCompany <- runDB $ selectFirst ([CompanyName ==. (search searchWrapper)] ||. 
                                       [CompanyStockId ==. (search searchWrapper)]) []
    
  -- TODO need to figure out how to do error
  let companyId = case maybeCompany of
                    (Just (Entity key _)) -> key
                    Nothing               -> Key {unKey = PersistInt64 (-1)}

  maybeCompanyStatus <- runDB $ selectFirst [CompanyStatusForCompany ==. companyId] []

  case maybeCompanyStatus of 
    (Just (Entity _ value)) -> gotCompany value
    Nothing                 -> errorPage


errorPage :: GHandler App App RepHtml
errorPage = do
  defaultLayout $ do
    setTitle "Finance web page"
    $(widgetFile "header")
    $(widgetFile "error")
    $(widgetFile "footer")
    addStylesheet $ StaticR css_bootstrap_css
    addStylesheet $ StaticR css_header_css
    addStylesheet $ StaticR css_general_css
    addStylesheet $ StaticR css_footer_css

gotCompany :: CompanyStatus -> GHandler App App RepHtml
gotCompany companyStatus = do
  let financeData         = searchTransform companyStatus
      inputs              = searchInputs financeData
      computedFinanceData = makeFinanceData financeData
      (expectedReturnAns, predictionTable, otherCalculations) = getAllStatistic computedFinanceData
  defaultLayout $ do
    setTitle "Finance web page"
    $(widgetFile "header")
    $(widgetFile "displayCalculation")
    $(widgetFile "footer")
    addStylesheet $ StaticR css_bootstrap_css
    addStylesheet $ StaticR css_header_css
    addStylesheet $ StaticR css_general_css
    addStylesheet $ StaticR css_footer_css

