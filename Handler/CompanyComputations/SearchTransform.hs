module Handler.CompanyComputations.SearchTransform (
  searchTransform,
  searchInputs
  ) where

import Import
import NewDataTypes
import FinanceTypeClass
import Handler.CompanyComputations.StatusDisplay (DisplayDataDouble)
import MakeFDataMethods (getFirstDataType)

searchTransform :: CompanyStatus -> [FinanceData]
searchTransform companyStatus = let
  pps  = Price_Per_Share               $ companyStatusPrice                         companyStatus
  as   = Assets                        $ companyStatusAssets                        companyStatus
  so   = Shares_Outstanding            $ companyStatusShares_Outstanding            companyStatus
  pere = Price_Earnings_Ratio_Estimate $ companyStatusPrice_Earnings_Ratio_Estimate companyStatus
  ncan = Non_Current_Assets_Now        $ companyStatusNon_Current_Assets_Now        companyStatus
  nca4 = Non_Current_Assets_4YearAgo   $ companyStatusNon_Current_Assets_4YearAgo   companyStatus
  pn   = Profit_Now                    $ companyStatusProfit_Now                    companyStatus
  p1   = Profit_1YearAgo               $ companyStatusProfit_1YearAgo               companyStatus
  p2   = Profit_2YearAgo               $ companyStatusProfit_2YearAgo               companyStatus
  p3   = Profit_3YearAgo               $ companyStatusProfit_3YearAgo               companyStatus
  dn   = Dividend_Now                  $ companyStatusDividend_Now                  companyStatus
  d1   = Dividend_1YearAgo             $ companyStatusDividend_1YearAgo             companyStatus
  d2   = Dividend_2YearAgo             $ companyStatusDividend_2YearAgo             companyStatus
  d3   = Dividend_3YearAgo             $ companyStatusDividend_3YearAgo             companyStatus
  en   = Equity_Now                    $ companyStatusEquity_Now                    companyStatus
  e1   = Equity_1YearAgo               $ companyStatusEquity_1YearAgo               companyStatus
  e2   = Equity_2YearAgo               $ companyStatusEquity_2YearAgo               companyStatus
  e3   = Equity_3YearAgo               $ companyStatusEquity_3YearAgo               companyStatus
  in pps : as : so : pere : ncan : nca4 : pn : p1 : p2 : p3 : dn : d1 : d2 : d3 : en : e1 : e2 : e3 : []

searchInputs :: [FinanceData] -> [DisplayDataDouble]
searchInputs financeData = let
  pps   = getFirstDataType isPricePerShare              financeData
  ass   = getFirstDataType isAssets                     financeData
  so    = getFirstDataType isSharesOutstanding          financeData
  peere = getFirstDataType isPriceEarningsRatioEstimate financeData
  ncan  = getFirstDataType isNonCurrentAssetsNow        financeData
  nca4  = getFirstDataType isNonCurrentAssets4YearAgo   financeData
  pn    = getFirstDataType isProfitNow                  financeData
  p1    = getFirstDataType isProfit1YearAgo             financeData
  p2    = getFirstDataType isProfit2YearAgo             financeData
  p3    = getFirstDataType isProfit3YearAgo             financeData
  dn    = getFirstDataType isDividendNow                financeData
  d1    = getFirstDataType isDividend1YearAgo           financeData
  d2    = getFirstDataType isDividend2YearAgo           financeData
  d3    = getFirstDataType isDividend3YearAgo           financeData
  en    = getFirstDataType isEquityNow                  financeData
  e1    = getFirstDataType isEquity1YearAgo             financeData
  e2    = getFirstDataType isEquity2YearAgo             financeData
  e3    = getFirstDataType isEquity3YearAgo             financeData
  numbList = pps : ass : so : peere : ncan : nca4 : pn : p1 : p2 : p3 : dn : d1 : d2 : d3 : en : e1 : e2 : e3 : []
  nameList = ["Price Per Share", "Assets", "Shares Outstanding", "Price Earnings Ratio Estimate",
              "Non Current Assets Now", "Non Current Assets 4 Years Ago", "Profit Now", "Profit 1 Year Ago", 
              "Profit 2 Year Ago", "Profit 3 Year Ago", "Dividend Now", "Dividend 1 Year Ago", 
              "Dividend 2 Year Ago", "Dividend 3 Year Ago", "Equity Now", "Equity 1 Year Ago", 
              "Equity 2 Year Ago", "Equity 3 Year Ago"]
  in zip nameList numbList
