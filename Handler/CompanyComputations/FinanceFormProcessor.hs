module Handler.CompanyComputations.FinanceFormProcessor (
  process, 
  getInputsAndStore
) where

import Import
import MakeFData (makeFinanceData)
import NewDataTypes
import Handler.Form.FinanceForm
import Data.Time.Clock (UTCTime)
import Handler.CompanyComputations.StatusDisplay (DisplayDataDouble)
import qualified Data.Text as T

-- FinanceForm is Maybe
process :: Maybe FinanceForm -> [FinanceData]
process maybeFormData = let
  fData = case maybeFormData of
         Just x  -> x
         Nothing -> error "process"
  parsed = parseFD fData
  in
  makeFinanceData parsed

parseFD :: FinanceForm -> [FinanceData]
parseFD formData = let
  pps  = Price_Per_Share               $ getTheDouble $ price_Per_Share               formData
  as   = Assets                        $ getTheDouble $ assets                        formData
  so   = Shares_Outstanding            $ getTheDouble $ shares_Outstanding            formData
  pere = Price_Earnings_Ratio_Estimate $ getTheDouble $ price_Earnings_Ratio_Estimate formData
  ncan = Non_Current_Assets_Now        $ getTheDouble $ non_Current_Assets_Now        formData
  nca4 = Non_Current_Assets_4YearAgo   $ getTheDouble $ non_Current_Assets_4YearAgo   formData
  pn   = Profit_Now                    $ getTheDouble $ profit_Now                    formData
  p1   = Profit_1YearAgo               $ getTheDouble $ profit_1YearAgo               formData
  p2   = Profit_2YearAgo               $ getTheDouble $ profit_2YearAgo               formData
  p3   = Profit_3YearAgo               $ getTheDouble $ profit_3YearAgo               formData
  dn   = Dividend_Now                  $ getTheDouble $ dividend_Now                  formData
  d1   = Dividend_1YearAgo             $ getTheDouble $ dividend_1YearAgo             formData
  d2   = Dividend_2YearAgo             $ getTheDouble $ dividend_2YearAgo             formData
  d3   = Dividend_3YearAgo             $ getTheDouble $ dividend_3YearAgo             formData
  en   = Equity_Now                    $ getTheDouble $ equity_Now                    formData
  e1   = Equity_1YearAgo               $ getTheDouble $ equity_1YearAgo               formData
  e2   = Equity_2YearAgo               $ getTheDouble $ equity_2YearAgo               formData
  e3   = Equity_3YearAgo               $ getTheDouble $ equity_3YearAgo               formData
  in
  pps : as : so : pere : ncan : nca4 : pn : p1 : p2 : p3 : dn : d1 : d2 : d3 : en : e1 : e2 : e3 : []
  where
    getTheDouble :: Text -> Double
    getTheDouble text = read (T.unpack text) :: Double

-- TODO Maybe should not be here
getInputsAndStore :: Maybe FinanceForm -> Key Company -> UTCTime -> ([DisplayDataDouble], CompanyStatus)
getInputsAndStore maybeFormData companyId now = let 

  formData = case maybeFormData of
               Just x  -> x
               Nothing -> error "error getInputsAndStore"
  pps   = getTheDouble $ price_Per_Share               formData
  ass   = getTheDouble $ assets                        formData
  so    = getTheDouble $ shares_Outstanding            formData
  peere = getTheDouble $ price_Earnings_Ratio_Estimate formData
  ncan  = getTheDouble $ non_Current_Assets_Now        formData
  nca4  = getTheDouble $ non_Current_Assets_4YearAgo   formData
  pn    = getTheDouble $ profit_Now                    formData
  p1    = getTheDouble $ profit_1YearAgo               formData
  p2    = getTheDouble $ profit_2YearAgo               formData
  p3    = getTheDouble $ profit_3YearAgo               formData
  dn    = getTheDouble $ dividend_Now                  formData
  d1    = getTheDouble $ dividend_1YearAgo             formData
  d2    = getTheDouble $ dividend_2YearAgo             formData
  d3    = getTheDouble $ dividend_3YearAgo             formData
  en    = getTheDouble $ equity_Now                    formData
  e1    = getTheDouble $ equity_1YearAgo               formData
  e2    = getTheDouble $ equity_2YearAgo               formData
  e3    = getTheDouble $ equity_3YearAgo               formData
  numbList = pps : ass : so : peere : ncan : nca4 : pn : p1 : p2 : p3 : dn : d1 : d2 : d3 : en : e1 : e2 : e3 : []
  nameList = ["Price Per Share", "Assets", "Shares Outstanding", "Price Earnings Ratio Estimate",
              "Non Current Assets Now", "Non Current Assets 4 Years Ago", "Profit Now", "Profit 1 Year Ago",
              "Profit 2 Year Ago", "Profit 3 Year Ago", "Dividend Now", "Dividend 1 Year Ago",
              "Dividend 2 Year Ago", "Dividend 3 Year Ago", "Equity Now", "Equity 1 Year Ago",
              "Equity 2 Year Ago", "Equity 3 Year Ago"]
  inputs = zip nameList numbList
  storeDB = CompanyStatus companyId pps ass so peere ncan nca4 pn p1 p2 p3 dn d1 d2 d3 en e1 e2 e3 now

  in (inputs, storeDB)
  where
    getTheDouble :: Text -> Double
    getTheDouble text = read (T.unpack text) :: Double
