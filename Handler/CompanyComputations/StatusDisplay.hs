module Handler.CompanyComputations.StatusDisplay (
  getAllStatistic,
  DisplayDataDouble
  ) where

import Import
import Prelude as P
import NewDataTypes
import Handler.NumberPrecision.NumberPrecision (createPercentage, precisionList)

type PredictionData = (Int,(Int,Int,Int))
type DisplayDataDouble = (String,Double)
type DisplayDataPercent = (String,Double)

getAllStatistic :: [FinanceData] -> (ExpectedReturn,[PredictionData],[DisplayDataPercent])
getAllStatistic financeData = let
  (expectedReturnAns, rest1) = getExpectedReturnAns financeData
  (predictionTable, rest2)   = getPredictionTable rest1
  otherCalculations          = getOtherCalculations rest2
  in (expectedReturnAns,predictionTable,otherCalculations)

getExpectedReturnAns :: [FinanceData] -> (ExpectedReturn,[FinanceData])
getExpectedReturnAns fdl = (ans,rest)
  where ans = createPercentage ((\(ExpectedReturnAns x) -> x) expectedReturn)
        expectedReturn = P.head fdl
        rest = P.tail fdl

getPredictionTable :: [FinanceData] -> ([PredictionData],[FinanceData])
getPredictionTable fdl = let
  financeTable = (getTable.(P.head)) fdl
  el = precisionList (equityList financeTable)
  pl = precisionList (profitList financeTable)
  fl = precisionList (dividendList financeTable)
  zipList      = zip3 el pl fl
  infoList     = P.zip [1::Int,2..] zipList
  rest         = P.tail fdl
  in (infoList,rest)
  where  
    getTable :: FinanceData -> FullTable
    getTable (ExpectedReturnTableAns x) = x
    getTable _ = error "error: getTable"

getOtherCalculations :: [FinanceData] -> [DisplayDataPercent]
getOtherCalculations fdl = let
  (epr,r1)   = getEPR fdl
  (ra, r2)   = getRA  r1
  (pra,r3)   = getPRA r2
  (fyt,r4)   = getFYT r3
  (yoy,r5)   = getYOY r4
  mc         = getMC  r5
  in epr:ra:pra:fyt:yoy:mc:[]
  where
    getEPR :: [FinanceData] -> (DisplayDataPercent,[FinanceData])
    getEPR fdl1 = (("ExpectedPayoutRatio",expectedPayoutRatio),rest)
      where expectedPayoutRatio = createPercentage (unwrap (P.head fdl1))
            unwrap (ExpectedPayoutRatioAns x) = x
            unwrap _ = error "error: getEPR"
            rest = P.tail fdl1

    getRA :: [FinanceData] -> (DisplayDataPercent,[FinanceData])
    getRA fdl2 = (("RoeAverage",roeAverage),rest)
      where roeAverage = createPercentage (unwrap (P.head fdl2))
            unwrap (RoeAverage x) = x
            unwrap _ = error "error: getRA"
            rest = P.tail fdl2

    getPRA :: [FinanceData] -> (DisplayDataPercent,[FinanceData])
    getPRA fdl3 = (("PayoutRationAverage",payoutRatioAverage),rest)
      where payoutRatioAverage = createPercentage (unwrap (P.head fdl3))
            unwrap (PayoutRatioAverageAns x) = x
            unwrap _ = error "error: getPRA"
            rest = P.tail fdl3

    getFYT :: [FinanceData] -> (DisplayDataPercent,[FinanceData])
    getFYT fdl4 = (("FourYearThing",fourYearThing),rest)
      where fourYearThing = createPercentage (unwrap (P.head fdl4))
            unwrap (FourYearThingAns x) = x
            unwrap _ = error "error: getFYT"
            rest = P.tail fdl4

    getYOY :: [FinanceData] -> (DisplayDataPercent,[FinanceData])
    getYOY fdl5 = (("YearOverYear",yearOverYear),rest)
      where yearOverYear = createPercentage (unwrap (P.head fdl5))
            unwrap (YearOverYearAns x) = x
            unwrap _ = error "error: getYOY"
            rest = P.tail fdl5

    getMC :: [FinanceData] -> DisplayDataPercent
    getMC fdl6 = ("MarketCapital",marketCapital)
      where marketCapital = createPercentage (unwrap (P.head fdl6))
            unwrap (MarketCapital x) = x
            unwrap _ = error "error: getMC"
