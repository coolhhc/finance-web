module Handler.NumberPrecision.NumberPrecision ( 
  precisionList,
  createPercentage
  ) where

--import Numeric 
import Prelude

--formatFloatN floatNum numOfDecimals = showFFloat (Just numOfDecimals) floatNum ""
precisionList :: [Double] -> [Int]
precisionList [] = []
precisionList (x:xs) = ((truncate x)::Int) : precisionList xs

createPercentage :: Double -> Double
createPercentage = precision01 . percentagefy

precision01 :: Double -> Double
precision01 num = final
  where temp1 = num * 100
        temp2 = truncate temp1
        final = (fromInteger temp2) / 100

percentagefy :: Double -> Double
percentagefy x = x * 100


