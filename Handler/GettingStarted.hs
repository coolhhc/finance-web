{-# LANGUAGE TupleSections, OverloadedStrings, QuasiQuotes #-}
module Handler.GettingStarted where

import Import

getGettingStartedR :: Handler RepHtml
getGettingStartedR = do
  defaultLayout $ do
    setTitle "Finance web page - getting started"
    addStylesheet $ StaticR css_bootstrap_css
    addStylesheet $ StaticR css_header_css
    addStylesheet $ StaticR css_general_css
    addStylesheet $ StaticR css_footer_css
    $(widgetFile "header")
    $(widgetFile "gettingstarted")
    $(widgetFile "footer")


